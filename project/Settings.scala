import sbt.Keys._
import sbt._
import play.sbt.PlayImport._

object Settings {

  //  lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")

  lazy val commonSettings = Seq(
    libraryDependencies ++= Seq(
      guice,
      specs2 % Test,
      jdbc,
      evolutions,
      "mysql" % "mysql-connector-java" % "8.0.25",
      "org.scalikejdbc" %% "scalikejdbc" % "3.5.0",
      "org.scalikejdbc" %% "scalikejdbc-config" % "3.5.0",
      "org.scalikejdbc" %% "scalikejdbc-play-initializer" % "2.8.0-scalikejdbc-3.5",
      "org.scalikejdbc" %% "scalikejdbc-test" % "3.5.0" % "test"
    ),
    Test / parallelExecution := true,
    Test / fork := true,
    Compile / scalaSource := baseDirectory.value / "src" / "main" / "scala",
//    Compile / TwirlKeys.compileTemplates / sourceDirectories := (Compile / unmanagedSourceDirectories).value,
    Test / scalaSource := baseDirectory.value / "src" / "test" / "scala",
    Compile / resourceDirectory := baseDirectory.value / "src" / "main" / "resources",
    Test / resourceDirectory := baseDirectory.value / "src" / "test" / "resources",
  )
}
