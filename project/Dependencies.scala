import sbt._

object Dependencies {

  // DB dependencies
  lazy val scalikejdbcVersion = "3.5.0"
  val scalikejdbc = "org.scalikejdbc" %% "scalikejdbc" % scalikejdbcVersion
  val scalikejdbcTest = "org.scalikejdbc" %% "scalikejdbc-test" % scalikejdbcVersion % Test
  val scalikejdbcConfig = "org.scalikejdbc" %% "scalikejdbc-config" % scalikejdbcVersion
  val scalikejdbcDbapiAdapter = "org.scalikejdbc" %% "scalikejdbc-play-dbapi-adapter" % "2.8.0-scalikejdbc-3.5"

  val skinnyOrm = "org.skinny-framework" %% "skinny-orm" % "3.0.3"

  val mysqlConnectorJava = "mysql" % "mysql-connector-java" % "8.0.11"


  val portDatabaseDependencies = Seq(
    scalikejdbc, scalikejdbcTest,
    scalikejdbcConfig, scalikejdbcDbapiAdapter,
    skinnyOrm, mysqlConnectorJava
  )

}
