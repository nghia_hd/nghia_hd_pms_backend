package module

import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule
import bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter.post.PostRepositoryOnJDBCImpl
import bbsManagement.domain.internal.post.PostRepository

class BBSModule extends AbstractModule with ScalaModule {
  lazy val postRepository: PostRepository = new PostRepositoryOnJDBCImpl

  override def configure(): Unit = {
    bind(classOf[PostRepository]).toInstance(postRepository)
  }
}