package bbsManagement.port.primary.webService.restAdapter.dto.post

import bbsManagement.domain.internal.post.model.Post

case class PostJson(
                   id: Option[Long],
                   title: String,
                   content: String,
                   )

object PostResponse {
  def apply(posts: Seq[Post]): Seq[PostJson] = posts.map(PostResponse(_))

  def apply(post: Post): PostJson = PostJson(
    id = Some(post.identifier.value),
    title = post.title,
    content = post.content
  )


}
