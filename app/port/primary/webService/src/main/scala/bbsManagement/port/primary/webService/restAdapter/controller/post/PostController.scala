package bbsManagement.port.primary.webService.restAdapter.controller.post

import javax.inject._
import bbsManagement.application.feature.service.post.PostService
import play.api.mvc._
import bbsManagement.port.primary.webService.restAdapter.controller.APIController
import bbsManagement.port.primary.webService.restAdapter.dto.post.PostResponse
import scala.util.{Failure, Success}



@Singleton
class PostController @Inject() (cc: ControllerComponents, postService: PostService )
  extends APIController(cc) {
  def list: Action[AnyContent] = Action {
    val result = postService.getAll
    result match {
      case Success(result) => success(PostResponse(result))
      case Failure(exception) => error(InternalServerError, 500, "Error! Can not get post list! \n" + exception.toString)
    }
  }
}
