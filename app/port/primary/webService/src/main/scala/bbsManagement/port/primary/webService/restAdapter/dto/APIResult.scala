package bbsManagement.port.primary.webService.restAdapter.dto


case class ErrorParameter(code: Int, message: String)

case class APIResult(
                      success: Boolean,
                      result: Option[Any] = None,
                      error: Option[ErrorParameter] = None)

object APIResult {
  def toSuccessJson(result: Any): APIResult = {
    APIResult(success = true, Some(result))
  }

  def toErrorJson(code: Int, message: String): APIResult = {
    APIResult(success = false, None, Some(ErrorParameter(code, message)))
  }

}
