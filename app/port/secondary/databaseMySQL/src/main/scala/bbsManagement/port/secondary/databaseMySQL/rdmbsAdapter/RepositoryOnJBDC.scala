package bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter

import scalikejdbc.{ AutoSession, DBSession }
import bbsManagement.utility.model
import bbsManagement.utility.repository._
import bbsManagement.utility.exception._

import scala.util.{ Success, Try }

trait BaseRepositoryOnJDBS[Identifier <: model.Identifier[_], Fields <: model.Fields, Entity <: model.Entity[Identifier], Record <: model.Record]
  extends Repository[Identifier, Fields, Entity] {

  protected def getDBSession(implicit ctx: IOContext): Try[DBSession] = {
    ctx match {
      case IOContextOnJDBC(session) => Success(session)
      case _ => Success(AutoSession)

    }
  }

  protected def withDBSession[T](func: DBSession => T)(implicit ctx: IOContext = IOContext): Try[T] = {
    getDBSession.map(func)
  }
}

trait RepositoryWithIdOnJDBC[Identifier <: model.Identifier[_], Fields <: model.Fields, Entity <: model.Entity[Identifier], RecordId, Record <: model.Record]
  extends BaseRepositoryOnJDBS[Identifier, Fields, Entity, Record] {
  type DAO <: CRUDMapperWithId[RecordId, Fields, Record]

  protected[this] val dao: DAO

  protected def convertToRecordId(identifier: Identifier): RecordId
}


trait ResolveAllWithIdFeatureRepositoryOnJDBC[Identifier <: model.Identifier[_], Fields <: model.Fields, Entity <: model.Entity[Identifier], RecordId, Record <: model.Record]
  extends RepositoryWithIdOnJDBC[Identifier, Fields, Entity, RecordId, Record]
    with ResolveAllFeatureRepository[Identifier, Fields, Entity] {

  protected def convertToEntity(record: Record): Entity

  def resolveAll()(implicit ctx: IOContext = IOContext): Try[Seq[Entity]] = withDBSession { implicit session =>
    dao.findAll().map(convertToEntity)
  }
}


trait BasicWithIdFeatureRepositoryOnJDBC[Identifier <: model.Identifier[_], Fields <: model.Fields, Entity <: model.Entity[Identifier], RecordId, Record <: model.Record]
  extends RepositoryWithIdOnJDBC[Identifier, Fields, Entity, RecordId, Record]
    with ResolveAllWithIdFeatureRepositoryOnJDBC[Identifier, Fields, Entity, RecordId, Record]

