package bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter.post

import bbsManagement.domain.internal.post.model.PostFields
import bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter.CRUDMapperWithId
import scalikejdbc.WrappedResultSet
import skinny.orm.Alias

private[post] class PostDao extends CRUDMapperWithId[Long, PostFields, PostRecord] {
  override lazy val defaultAlias: Alias[PostRecord] = createAlias("post")
  override lazy val tableName = "post"
  override lazy val primaryKeyFieldName = "id"

  override def columnNames: Seq[String] = Seq(
    "id",
    "title",
    "content"
  )

  override def idToRawValue(id: Long): Long = id

  override def rawValueToId(value: Any): Long = value.asInstanceOf[Long]

  override def toNamedValues(entity: PostRecord): Seq[(Symbol, Any)] = Seq(
    Symbol("id") -> entity.id,
    Symbol("title") -> entity.title,
    Symbol("content") -> entity.content
  )

  override def extract(rs: WrappedResultSet, n: scalikejdbc.ResultName[PostRecord]): PostRecord =
    PostRecord(
      id = rs.get(n.id),
      title = rs.get(n.title),
      content = rs.get(n.content)
    )
}
