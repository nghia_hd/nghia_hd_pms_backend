package bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter

import scalikejdbc.DBSession
import bbsManagement.utility.repository.IOContext

case class IOContextOnJDBC(session: DBSession) extends IOContext
