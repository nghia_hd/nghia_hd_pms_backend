package bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter

import skinny.orm.SkinnyMapperBase

trait CRUDMapper[ObjectFields, Entity] extends SkinnyMapperBase[Entity] {
  def toNamedValues(entity: Entity): Seq[(Symbol, Any)]

}
