package bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter.post

import bbsManagement.utility.model.Record
import org.joda.time.DateTime

case class PostRecord(
                       id: Long,
                       title: String,
                       content: String) extends Record

