package bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter.post

import bbsManagement.domain.internal.post.PostRepository
import bbsManagement.domain.internal.post.model.{ Post, PostFields, PostId }
import bbsManagement.port.secondary.databaseMySQL.rdmbsAdapter.BasicWithIdFeatureRepositoryOnJDBC
import bbsManagement.utility.exception.EntityNotFoundException

import scala.util.Try

class PostRepositoryOnJDBCImpl(override protected[this] val dao: PostDao = new PostDao)
  extends PostRepository with BasicWithIdFeatureRepositoryOnJDBC[PostId, PostFields, Post, Long, PostRecord] {

  override type DAO = PostDao

  override protected def convertToEntity(record: PostRecord): Post = {
    Post(
      identifier = PostId(record.id),
      title = record.title,
      content = record.content
      )
  }

  override def resolveAllActive(): Try[Seq[Post]] = withDBSession {
    implicit session => dao.findAllWithLimitOffset().map(convertToEntity)
  }

  override protected def convertToRecordId(identifier: PostId): Long = identifier.value
}
