package bbsManagement.domain.internal.post.model

import bbsManagement.utility.model.Identifier

case class PostId(value: Long) extends Identifier[Long]