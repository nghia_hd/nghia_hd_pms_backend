package bbsManagement.domain.internal.post.model

import org.joda.time.DateTime

case class PostImpl (
                    identifier: PostId = PostId.apply(0),
                    title: String,
                    content: String
                    ) extends Post
