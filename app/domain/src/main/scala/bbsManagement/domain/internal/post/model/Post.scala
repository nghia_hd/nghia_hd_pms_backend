package bbsManagement.domain.internal.post.model

import org.joda.time.DateTime
import bbsManagement.utility.model.Entity

trait Post extends Entity[PostId] with PostFields

object Post {

  def apply(
           identifier: PostId,
           title: String,
           content: String): Post = PostImpl(
           identifier,
           title,
           content)

  def unapply(arg: Post): Option[Nothing] = None
}
