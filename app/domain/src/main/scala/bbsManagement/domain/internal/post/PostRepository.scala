package bbsManagement.domain.internal.post

import bbsManagement.domain.internal.post.model.{Post, PostFields, PostId}

import scala.util.Try

trait PostRepository{
  def resolveAllActive(): Try[Seq[Post]]

}
