package bbsManagement.domain.internal.post.model

import org.joda.time.DateTime
import bbsManagement.utility.model.Fields

trait PostFields extends Fields {
  val title: String
  val content: String
}
