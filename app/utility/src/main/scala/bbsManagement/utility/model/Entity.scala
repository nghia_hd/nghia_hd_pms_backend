package bbsManagement.utility.model

trait Entity[ID <: Identifier[_]] {
  val identifier: ID
}