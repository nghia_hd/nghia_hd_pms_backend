package bbsManagement.utility.exception

class OptimisticLockException(message: String, cause: Option[Throwable])
  extends BaseException(message, cause)
