package bbsManagement.utility.exception

class EntityNotFoundException(message: String, cause: Option[Throwable] = None)
  extends BaseException(message, cause)
