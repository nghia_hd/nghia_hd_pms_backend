package bbsManagement.application.feature.service.post

import bbsManagement.domain.internal.post.model.Post
import bbsManagement.domain.internal.post.PostRepository

import javax.inject._
import scala.util.Try

@Singleton
class PostService @Inject() (postRepository: PostRepository) {
  def getAll: Try[Seq[Post]] = postRepository.resolveAllActive()
}
