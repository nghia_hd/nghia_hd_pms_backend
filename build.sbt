import sbt._
import Keys._
import Dependencies._
import Settings._
import com.typesafe.config._

ThisBuild / scalaVersion := "2.13.7"
name := "bbs"
ThisBuild / version := "0.1"
ThisBuild / organization := "Flinters-vietnam"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(SbtTwirl)
  .aggregate(application, domain, port, utility)
  .dependsOn(application, domain, port, utility)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(
    "com.pauldijou" %% "jwt-play" % "5.0.0",
    "com.pauldijou" %% "jwt-core" % "5.0.0",
    "com.auth0" % "jwks-rsa" % "0.20.0",
    "net.codingwell" %% "scala-guice" % "5.0.2"
  ))

lazy val application = (project in file("app/application"))
  .enablePlugins(PlayScala)
  .enablePlugins(SbtTwirl)
  .dependsOn(domain, utility)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(
    "org.json4s" %% "json4s-native" % "4.0.2" exclude("com.fasterxml.jackson.core", "jackson-databind"),
    "org.json4s" %% "json4s-jackson" % "4.0.2" exclude("com.fasterxml.jackson.core", "jackson-databind"),
  ))


lazy val domain = (project in file("app/domain"))
  .enablePlugins(PlayScala)
  .dependsOn(utility)
  .settings(commonSettings: _*)


lazy val port = (project in file("app/port"))
  .enablePlugins(PlayScala)
  .aggregate(portWebService, portDatabase, portHttp)
  .dependsOn(portWebService, portDatabase, portHttp)
  .settings(commonSettings: _*)


lazy val portWebService = (project in file("app/port/primary/webService"))
  .enablePlugins(PlayScala)
  .dependsOn(application, utility)
  .settings(commonSettings: _*)


lazy val portDatabase = (project in file("app/port/secondary/databaseMySQL"))
  .enablePlugins(PlayScala)
  .settings(libraryDependencies ++= portDatabaseDependencies)
  .dependsOn(utility, application)
  .settings(commonSettings: _*)


lazy val portHttp = (project in file ("app/port/secondary/http"))
  .enablePlugins(PlayScala)
  .dependsOn(utility, application)
  .settings(commonSettings: _*)


lazy val utility = (project in file("app/utility"))
  .enablePlugins(PlayScala)
  .settings()
  .settings(commonSettings: _*)
